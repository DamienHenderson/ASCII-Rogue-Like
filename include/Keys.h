#ifndef KEYS_H
#define KEYS_H
/*
                COMBINATIONS USING JUST THE 'GREY' KEYS:


                key[F1]        = '^[[[A'
                key[F2]        = '^[[[B'
                key[F3]        = '^[[[C'
                key[F4]        = '^[[[D'
                key[F5]        = '^[[[E'
                key[F6]        = '^[[17~'
                key[F7]        = '^[[18~'
                key[F8]        = '^[[19~'
                key[F9]        = '^[[20~'
                key[F10]       = '^[[21~'
                key[F11]       = '^[[23~'
                key[F12]       = '^[[24~'


                key[Shift-F1]  = '^[[25~'
                key[Shift-F2]  = '^[[26~'
                key[Shift-F3]  = '^[[28~'
                key[Shift-F4]  = '^[[29~'
                key[Shift-F5]  = '^[[31~'
                key[Shift-F6]  = '^[[32~'
                key[Shift-F7]  = '^[[33~'
                key[Shift-F8]  = '^[[34~'


                key[Insert]    = '^[[2~'
                key[Delete]    = '^[[3~'
                key[Home]      = '^[[1~'
                key[End]       = '^[[4~'
                key[PageUp]    = '^[[5~'
                key[PageDown]  = '^[[6~'
                key[Up]        = '^[[A'
                key[Down]      = '^[[B'
                key[Right]     = '^[[C'
                key[Left]      = '^[[D'


                key[Bksp]      = '^?'
                key[Bksp-Alt]  = '^[^?'
                key[Bksp-Ctrl] = '^H'    console only.


                key[Esc]       = '^['
                key[Esc-Alt]   = '^[^['


                key[Enter]     = '^M'
                key[Enter-Alt] = '^[^M'


                key[Tab]       = '^I' or '\t'  unique form! can be bound, but does not 'showkey -a'.
                key[Tab-Alt]   = '^[\t'


                COMBINATIONS USING THE WHITE KEYS:


                Anomalies:
                'Ctrl+`' == 'Ctrl+2', and 'Ctrl+1' == '1' in xterm.
                Several 'Ctrl+number' combinations are void at console, but return codes in xterm. OTOH Ctrl+Bksp returns '^H' at console, but is identical to plain 'Bksp' in xterm. There are no doubt more of these little glitches however, in the main:


                White key codes are easy to undertand, each of these 'normal' printing keys has six forms:


                A            = 'a'    (duhhh)
                A-Shift      = 'A'    (who would have guessed?)
                A-Alt        = '^[a'

                A-Ctrl       = '^A'
                A-Alt-Ctrl   = '^[^A'
                A-Alt-Shift  = '^[A'
                A-Ctrl-Shift = '^A'   (Shift has no effect)


                Don't forget that:


                /-Shift-Ctrl = Bksp      = '^?'
                [-Ctrl       = Esc       = '^['
                M-Ctrl       = Enter     = '^M'
                */
    enum class Keys
    {
        // This was not fun to type out
        Escape = 0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,

        Left_Tick, Numeric_1, Exclamation_Mark, Numeric_2, Double_Quote,

        Numeric_3, Pound_Sign, Numeric_4, Dollar_Sign, Numeric_5,

        Percentage_Sign, Numeric_6, Caret, Numeric_7, Ampersand, Numeric_8, Asterix,

        Numeric_9, Left_Bracket, Numeric_0, Right_Bracket, Minus_Sign, Underscore, Equal_Sign, Plus_Sign, Backspace,

        Tab, q, Q, w, W, e, E, r, R, t, T, y, Y, u, U, i, I, o, O, p, P, Left_Square_Bracket, Left_Curly_Brace, Right_Square_Bracket, Right_Curly_Brace,

        Return, Caps_Lock, a, A, s, S, d, D, f, F, g, G, h, H, j, J, k, K, l, L, SemiColon, Colon, Single_Quote, At_Symbol, Hash, Tilde,

        BackSlash, Vertical_Bar, z, Z, x, X, c, C, v, V, b, B, n, N, m, M, Comma, Left_Angle_Bracket, Dot, Right_Angle_Bracket, ForwardSlash, Question_Mark,

        Insert, Home, PageUp, Delete, End, PageDown, Left_Arrow, Right_Arrow, Up_Arrow, Down_Arrow, Unknown

    };
    Keys Convert_Char_To_Keys_Enum(char in);
#endif // KEYS_H
