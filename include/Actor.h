#ifndef ACTOR_H
#define ACTOR_H

#include "common.h"
#include "types.h"
class Actor
{
    public:
        Actor(int2 initialposition, int startinghealth, char character);
        virtual ~Actor();
        int2 GetPosition();
        void SetPostion(int2 newpos);
        void SetHealth(int newhealth);
        int  GetHealth();
        char GetActorChar();
        void SetActorChar(char newchar);
    protected:

    private:
        int2 position;
        int health;
        char Actor_char;


};

#endif // ACTOR_H
