#ifndef DH_TYPES_H
#define DH_TYPES_H


struct int2
{
int x;
int y;
};

bool operator < (const int2 &l, const int2 &r);


#endif
