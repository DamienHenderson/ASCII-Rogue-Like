#ifndef SCREENBUFFER_H
#define SCREENBUFFER_H
#include "common.h"
#include "types.h"
class ScreenBuffer
{
    public:
        ScreenBuffer(std::vector<std::string> intitalbuffer);
        virtual ~ScreenBuffer();
        int2 GetDimensions();
        void EditChar(int2 index,char newchar);
        char GetChar(int2 index);
        std::vector<std::string> GetBuffer();

    protected:

    private:
    std::vector<std::string> buffer;
    int2 dimensions;
};

#endif // SCREENBUFFER_H
