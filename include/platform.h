#ifndef DH_PLATFORM_H
#define DH_PLATFORM_H
#include "common.h"
#ifdef __linux__
#define CLEARSCREEN std::cout << "\033[2J\033[1;1H";

#elif defined(__MINGW32__)
#define CLEARSCREEN system("cls");


#endif

#ifdef __linux__

#elif defined(_Win32)

#endif

#ifdef __linux__


/**
 Linux (POSIX) implementation of _kbhit().
 Morgan McGuire, morgan@cs.brown.edu
 */
#include <stdio.h>
#include <sys/select.h>
#include <termios.h>
#include <sys/ioctl.h>

int _kbhit();
std::string getcharactersequence();

#elif defined(__MINGW32__)

#include <conio.h>
// fix std::to_string being missing on mingw
namespace std
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}
#endif // __linux__

#ifdef __linux__

// Terminal Colours for Linux
#endif
#endif
