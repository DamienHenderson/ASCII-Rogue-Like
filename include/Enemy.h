#ifndef ENEMY_H
#define ENEMY_H

#include "Actor.h"


class Enemy : public Actor
{
    public:
        Enemy();
        virtual ~Enemy();
        void DoAIMove();
    protected:

    private:
};

#endif // ENEMY_H
