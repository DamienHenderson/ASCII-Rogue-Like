#ifndef MAP_H
#define MAP_H
#include "common.h"
#include "platform.h"
#include "types.h"
#include "ScreenBuffer.h"
class Map
{
    public:
        Map(const std::vector<std::string>& data, int linewidth);
        std::vector<std::string> GetMapData();
        virtual ~Map();
        char getMapCell(int2 index);
        void updateMapCell(int2 index , char new_value);
        int getMapCellFog(int2 index);
        void updateMapCellFog(int2 index , int new_value);

    protected:

    private:
        std::vector<std::string> mapdata;
        std::vector<std::vector<int> > fog;
};

#endif // MAP_H
