#ifndef GAME_H
#define GAME_H

#include "Map.h"
#include "types.h"
#include "Colours.h"
#include "common.h"
#include "Actor.h"
#include "States.h"
#include "Keys.h"
class Game
{
    public:
        Game(std::vector<std::string>& data);
        void PrintScreenBuffer();
        void PrintMap();
        virtual ~Game();
        bool GetGameIsRunning();
        void ProcessInput(Keys input);
        void Run();
    protected:

    private:

        void populatemap(std::vector<std::string> data);
        void processmove(int2 newpos,Keys action);
        Map* gamemap;
        ScreenBuffer* scr_buffer;
        Actor * player;
        bool running;
        bool player_has_key;
        bool isDirty;
        int turn_counter;
        std::queue<std::string> message_queue;
        PlayerState player_state;

};


#endif // GAME_H
