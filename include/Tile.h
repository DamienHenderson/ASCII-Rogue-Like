
#ifndef TILE_H
#define TILE_H
#include "types.h"
struct Tile
{
    int     ID;
    int     itemID;
    bool    isPassable;
    char    displayedCharacter;
    bool    isOccupied;

};
#endif
