#ifndef COLOURS_H
#define COLOURS_H
//the following are UBUNTU/LINUX ONLY terminal color codes.
#ifdef __linux__
#define RESET   std::cout << "\033[0m";
#define BLACK   std::cout << "\033[30m";     /* Black */
#define RED     std::cout << "\033[31m" ;     /* Red */
#define GREEN   std::cout << "\033[32m";      /* Green */
#define YELLOW  std::cout << "\033[33m" ;     /* Yellow */
#define BLUE    std::cout << "\033[34m" ;     /* Blue */
#define MAGENTA std::cout << "\033[35m";      /* Magenta */
#define CYAN    std::cout << "\033[36m" ;     /* Cyan */
#define WHITE   std::cout << "\033[37m" ;     /* White */
#define BOLDBLACK   std::cout << "\033[1m\033[30m" ;     /* Bold Black */
#define BOLDRED     std::cout << "\033[1m\033[31m";     /* Bold Red */
#define BOLDGREEN   std::cout << "\033[1m\033[32m";      /* Bold Green */
#define BOLDYELLOW  std::cout << "\033[1m\033[33m" ;     /* Bold Yellow */
#define BOLDBLUE    std::cout << "\033[1m\033[34m";      /* Bold Blue */
#define BOLDMAGENTA std::cout << "\033[1m\033[35m";      /* Bold Magenta */
#define BOLDCYAN    std::cout << "\033[1m\033[36m" ;     /* Bold Cyan */
#define BOLDWHITE   std::cout << "\033[1m\033[37m";      /* Bold White */

#elif defined(__MINGW32__)
#define RESET   //std::cout << "\033[0m";
#define BLACK  // std::cout << "\033[30m";     /* Black */
#define RED    // std::cout << "\033[31m" ;     /* Red */
#define GREEN //  std::cout << "\033[32m";      /* Green */
#define YELLOW // std::cout << "\033[33m" ;     /* Yellow */
#define BLUE  //  std::cout << "\033[34m" ;     /* Blue */
#define MAGENTA// std::cout << "\033[35m";      /* Magenta */
#define CYAN   // std::cout << "\033[36m" ;     /* Cyan */
#define WHITE  // std::cout << "\033[37m" ;     /* White */
#define BOLDBLACK //  std::cout << "\033[1m\033[30m" ;     /* Bold Black */
#define BOLDRED   //  std::cout << "\033[1m\033[31m";     /* Bold Red */
#define BOLDGREEN  // std::cout << "\033[1m\033[32m";      /* Bold Green */
#define BOLDYELLOW // std::cout << "\033[1m\033[33m" ;     /* Bold Yellow */
#define BOLDBLUE   // std::cout << "\033[1m\033[34m";      /* Bold Blue */
#define BOLDMAGENTA// std::cout << "\033[1m\033[35m";      /* Bold Magenta */
#define BOLDCYAN   // std::cout << "\033[1m\033[36m" ;     /* Bold Cyan */
#define BOLDWHITE  // std::cout << "\033[1m\033[37m";      /* Bold White */
#endif
#endif // COLOURS_H
