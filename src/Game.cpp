#include "Game.h"

Game::Game(std::vector<std::string>& data)
{
    populatemap(data);
    running = true;

    this->message_queue.push("Welcome to my game");
    this->message_queue.push("You awake to find yourself in a dark room");
    this->message_queue.push("------------RogueLike------------");
    this->player_state = PlayerState::Moving;
    // set player position

    for(unsigned int iter = 0; iter < gamemap->GetMapData().size(); iter ++)
    {
        for( unsigned int i = 0; i < gamemap->GetMapData().at(iter).length(); i++)
        {
            if ( gamemap->GetMapData()[iter].at(i) == 'P')
            {
                int2 player_pos;
                player_pos.x = i;
                player_pos.y = iter;
                this->player = new Actor(player_pos,10,'@');
                iter = gamemap->GetMapData().size();
                break;
            }
            else
            {
                // std::cout << i << " " << iter << " ";
                // std::cout << gamemap->GetMapData()[iter].at(i) << std::endl;

            }
        }

        // std::cout << it.first.x << " " << it.first.y;
    }

    player_has_key = false;
    isDirty = true;
    this->turn_counter = 0;

    //ctor
}

Game::~Game()
{
    //dtor
}
void Game::Run()
{

    Keys pressed_key = Keys::Unknown;
    char buffer;

    if (isDirty)
    {
        CLEARSCREEN

        std::cout << std::endl;
        BOLDCYAN
        if (this->message_queue.size() > 1)
        {
            // Only Pop if there are at least 2 items in the queue
            this->message_queue.pop();
        }
        std::cout << this->message_queue.front() << std::endl;



        BOLDGREEN
        std::cout << "Controls: W = move north , A = move west , S = move south and D = move east." << std::endl;
        BOLDRED
        std::cout << "Health: " << player->GetHealth() << std::endl;
        BOLDYELLOW
        std::cout << "Turn: " << this->turn_counter << std::endl;
        RESET

        if (player->GetHealth() <= 0)
        {
            std::cout << "You have died! Start Again? [Y]es [N]o" << std::endl;



            std::cin >> buffer;

            if (buffer == 'n' || buffer == 'N')
            {
                isDirty = false;
                running = false;
            }


        }
        else
        {
            this->PrintMap();


        }
        isDirty = false;
    }
    //set_stdin(false);
    //sleep(20);
    //std::string buffer_log;

    if(_kbhit())
    {
        buffer = getchar();
       // std::cout << buffer;
        //buffer_log += buffer;
        if (buffer == '\033')
        {

            // check if there are still bytes in stdin
            if (_kbhit())
            {
                // check the next character to see if it is a [
                buffer = getchar();
                //buffer_log += buffer;
                std::cout << buffer;
                if(buffer == '[')
                {

                    if (_kbhit())
                    {
                        buffer = getchar();
                        std::cout << buffer;
                        switch(buffer)
                        {
                        case 'A':
                            // Up Arrow
                            buffer = 'W';
                            pressed_key = Keys::Up_Arrow;
                            break;
                        case 'B':
                            // Down Arrow
                            buffer = 'S';
                            pressed_key = Keys::Down_Arrow;
                            break;
                        case 'C':
                            // Right Arrow
                            buffer = 'D';
                            pressed_key = Keys::Right_Arrow;
                            break;
                        case 'D':
                            // Left Arrow
                            buffer = 'A';
                            pressed_key = Keys::Left_Arrow;
                            break;

                        case 'H':
                            // Home Key
                            //std::cout << "Home Key";
                            //this->message_queue.push("Home Key");
                            pressed_key = Keys::Home;
                            break;
                        case '5':
                            if (_kbhit())
                            {
                                buffer = getchar();
                                std::cout << buffer;
                                //  buffer_log += buffer;
                                if(buffer == '~')
                                {
                                    //  this->message_queue.push("End Key");
                                    pressed_key = Keys::PageUp;
                                }
                            }
                            break;
                        case 'F':

                            //  this->message_queue.push("End Key");
                            pressed_key = Keys::End;


                            break;
                        case '6':
                            if (_kbhit())
                            {
                                buffer = getchar();
                                //std::cout << buffer;
                                //  buffer_log += buffer;
                                if(buffer == '~')
                                {
                                    //  this->message_queue.push("Page Down Key");
                                    pressed_key = Keys::PageDown;
                                }
                            }
                            break;

                        default:

                            std::cout << buffer;
                            break;
                        }
                    }







                }


                //std::cout << buffer_log << std::endl;


                //this->player_state = PlayerState::Moving;

            }
            else
            {
                // if there are no bytes waiting then it is just escape so exit
                pressed_key = Keys::Escape;

            }
            this->ProcessInput(pressed_key);
            isDirty = true;
            if (this->player_state == PlayerState::Moving)
            {
                this->turn_counter++;
            }

        }
        else
        {
            pressed_key = Convert_Char_To_Keys_Enum(buffer);
            this->ProcessInput(pressed_key);
            isDirty = true;
            if (this->player_state == PlayerState::Moving)
            {
                this->turn_counter++;
            }
        }




    }
}

bool Game::GetGameIsRunning()
{
    return running;
}
void Game::populatemap(std::vector<std::string> data)
{

    gamemap = new Map(data,10);
}
void Game::PrintScreenBuffer()
{
    std::vector<std::string> scr_buf = scr_buffer->GetBuffer();

    for (auto const& it : scr_buf)
    {
        // NOTE: should screen buffers contain newline characters or should i do newlines manually here?
        std::cout << it;
    }
}
void Game::PrintMap()
{
    int linepos =0;
    std::vector<std::string> mapdata = gamemap->GetMapData();
    for(unsigned int it = 0; it < mapdata.size(); it++)
    {
        for (unsigned int i = 0; i < mapdata[it].length(); i++)
        {
            switch(mapdata[it].at(i))
            {
            case ' ':
                // Empty Tile
                WHITE
                std::cout << " ";
                RESET
                break;
            case 'W':
                // Wall
                BOLDBLACK
                std::cout << "#";
                RESET
                break;
            case 'F':
                // False Wall
                BOLDBLACK
                std::cout << "#";
                RESET
                break;
            case 'G':
                // Goal
                GREEN
                std::cout << "~";
                RESET
                break;
            case 'P':
                // Player
                BOLDBLUE
                std::cout << player->GetActorChar();
                RESET
                break;
            case 'D':
                // Closed Door
                YELLOW
                std::cout << "+";
                RESET
                break;
            case '\n':
                RESET
                std::cout << '\n';
                break;
            case 'K':
                // Key
                YELLOW
                std::cout << "*";
                RESET
                break;
            // case '\\':
            // Open Door
            //WHITE
            // std::cout << '\\';
            // RESET;
            // break;
            default:
                // Tile not recognised
                BOLDRED
                std::cout << "!";
                RESET
                break;
            }
            linepos ++;
        }
    }
}
void Game::processmove(int2 newpos, Keys action)
{
    char cell = gamemap->getMapCell(newpos);
    switch (cell)
    {
    case 'W':
    case 'w':
        this->message_queue.push("You cannot walk through a wall");

        break;
    case 'F':
    {
        int2 newpos_temp = newpos;
        this->message_queue.push("There is a wall there it seems Odd.\nYou try to walk through it and you go straight through.\nThe Wall seems to be made of paper.");
        switch (action)
        {
        case Keys::W:
        case Keys::w:
        case Keys::Up_Arrow:
            newpos_temp.y -= 1;
            break;
        case Keys::A:
        case Keys::a:
        case Keys::Left_Arrow:
            newpos_temp.x -= 1;
            break;
        case Keys::S:
        case Keys::s:
        case Keys::Down_Arrow:
            newpos_temp.y += 1;
            break;
        case Keys::D:
        case Keys::d:
        case Keys::Right_Arrow:
            newpos_temp.x += 1;
            break;
        default:
            break;
        }
        if(gamemap->getMapCell(newpos_temp) != 'W' && gamemap->getMapCell(newpos_temp) != 'D')
        {
            gamemap->updateMapCell(player->GetPosition(), ' ');
            gamemap->updateMapCell(newpos_temp,'P');
            this->player->SetPostion(newpos_temp);
        }
        else
        {
            this->message_queue.push("There is something on the other side of the False Wall");

        }

        break;
    }
    case 'G':
    case 'g':
        this->message_queue.push("You have reached the Goal");
        break;

    case 'D':
    case 'd':
    {
        std::cout << std::endl << "There is a door there Press [E] to try to open it, Press [K] to use the key or press [X] to move away.";
        this->player_state = PlayerState::atDoor;
        int newhealth = player->GetHealth();
        int2 newpos_temp = newpos;
        char in;
        std::cin >> in;
        switch (in)
        {
        case 'E':
        case 'e':
        {
            newhealth -= 4;

            this->player->SetHealth(newhealth);
            this->message_queue.push("You kick open the door, it swings open with a sound of breaking wood.\nHowever, you kicked right into a nail taking 4 points of damage.");

            gamemap->updateMapCell(player->GetPosition(), ' ');
            // Destroy the Door because the player has kicked it open
            gamemap->updateMapCell(newpos, ' ');

            switch (action)
            {
            case Keys::W:
            case Keys::w:
            case Keys::Up_Arrow:
                newpos_temp.y -= 1;
                break;
            case Keys::A:
            case Keys::a:
            case Keys::Left_Arrow:
                newpos_temp.x -= 1;
                break;
            case Keys::S:
            case Keys::s:
            case Keys::Down_Arrow:
                newpos_temp.y += 1;
                break;
            case Keys::D:
            case Keys::d:
            case Keys::Right_Arrow:
                newpos_temp.x += 1;
                break;
            default:
                this->message_queue.push("Invalid Action");
                break;
            }
            gamemap->updateMapCell(newpos_temp,'P');
            this->player->SetPostion(newpos_temp);


            break;
            case 'K':
            case 'k':
            {
                if (player_has_key)
                {
                    this->message_queue.push("Well done you opened a door and you didn't even have to kick a nail.\nThe Door Slams shut behind you.");



                    switch (action)
                    {
                    case Keys::W:
                    case Keys::w:
                    case Keys::Up_Arrow:
                        newpos_temp.y -= 1;
                        break;
                    case Keys::A:
                    case Keys::a:
                    case Keys::Left_Arrow:
                        newpos_temp.x -= 1;
                        break;
                    case Keys::S:
                    case Keys::s:
                    case Keys::Down_Arrow:
                        newpos_temp.y += 1;
                        break;
                    case Keys::D:
                    case Keys::d:
                    case Keys::Right_Arrow:
                        newpos_temp.x += 1;
                        break;
                    default:
                        this->message_queue.push("Invalid Action.");
                        break;
                    }
                    if(gamemap->getMapCell(newpos_temp) != 'W' && gamemap->getMapCell(newpos_temp) != 'D')
                    {
                        gamemap->updateMapCell(player->GetPosition(), ' ');
                        gamemap->updateMapCell(newpos_temp,'P');
                        this->player->SetPostion(newpos_temp);
                        this->player_state = PlayerState::Moving;
                    }
                    else
                    {
                        this->message_queue.push("There is something on the other side of the Door");

                    }

                    break;
                }
                else
                {
                    this->message_queue.push("You fumble around with the lock for a moment until you realise you don't have a key.");
                    break;
                }
            }
            default:
                this->player_state = PlayerState::atDoor;
                break;
            }
        }


        break;
    }


    case 'K':
    case 'k':
    {
        this->message_queue.push("You found a key no more kicking nails for you!");
        // set current position to ' '
        // place 'P' at new position
        gamemap->updateMapCell(player->GetPosition(),' ');
        gamemap->updateMapCell(newpos,'P');
        this->player->SetPostion(newpos);
        player_has_key = true;
        this->player_state = PlayerState::Moving;
        break;
    }
    default:
    {
        // set current position to ' '
        // place 'P' at new position
        gamemap->updateMapCell(player->GetPosition(),' ');
        gamemap->updateMapCell(newpos,'P');
        this->player->SetPostion(newpos);
        //this->message_queue.push("New position X: " + std::to_string(this->player->GetPosition().x) + " Y: " + std::to_string(this->player->GetPosition().y));
        this->player_state = PlayerState::Moving;

        break;
    }


    }
}
void Game::ProcessInput(Keys input)
{

    int2 temp_pos = this->player->GetPosition();
    //std::cout << input;

    switch (input)
    {
    case Keys::W:
    case Keys::w:
        // go north so subtract 1 from the y coordinate
        temp_pos.y -= 1;
        //std::cout << temp_pos.x << " " << temp_pos.y << std::endl;
        processmove(temp_pos,input);


        break;
    case Keys::A:
    case Keys::a:
        // go west so subtract 1 from the x coordinate
        temp_pos.x -= 1;
        processmove(temp_pos,input);



        break;


    case Keys::S:
    case Keys::s:
        // go south so add 1 to the y coordinate
        temp_pos.y += 1;
        //std::cout << temp_pos.x << " " << temp_pos.y << std::endl;
        processmove(temp_pos,input);



        break;

    case Keys::D:
    case Keys::d:
        // go east so add 1 to the x coordinate
        temp_pos.x += 1;
        processmove(temp_pos,input);



        break;
    case Keys::Up_Arrow:

        // go north so subtract 1 from the y coordinate
        temp_pos.y -= 1;
        //std::cout << temp_pos.x << " " << temp_pos.y << std::endl;
        processmove(temp_pos,input);


        break;
    case Keys::Left_Arrow:

        // go west so subtract 1 from the x coordinate
        temp_pos.x -= 1;
        processmove(temp_pos,input);



        break;


    case Keys::Down_Arrow:

        // go south so add 1 to the y coordinate
        temp_pos.y += 1;
        //std::cout << temp_pos.x << " " << temp_pos.y << std::endl;
        processmove(temp_pos,input);



        break;

    case Keys::Right_Arrow:

        // go east so add 1 to the x coordinate
        temp_pos.x += 1;
        processmove(temp_pos,input);



        break;
    case Keys::Left_Tick:
        // Commands here later
        // Die for now
        player->SetHealth(0);
        break;
    case Keys::Q:
    case Keys::q:
    case Keys::Home:
        // go North West so subtract 1 from both components
        temp_pos.x -= 1;
        temp_pos.y -= 1;
        processmove(temp_pos,input);
        break;
    case Keys::E:
    case Keys::e:
    case Keys::PageUp:
        // go North East so subtract 1 from the y componet and add 1 to the x component
        temp_pos.x += 1;
        temp_pos.y -= 1;
        processmove(temp_pos,input);
        break;
    case Keys::Z:
    case Keys::z:
    case Keys::End:
        // go South West so subtract 1 from the x component and add 1 to the y component
        temp_pos.x -= 1;
        temp_pos.y += 1;
        processmove(temp_pos,input);
        break;
    case Keys::C:
    case Keys::c:
    case Keys::PageDown:
        // go South East so add 1 to both components
        temp_pos.x += 1;
        temp_pos.y += 1;
        processmove(temp_pos,input);
        break;
    case Keys::Escape:
        running = false;
        break;
    default:
        break;
    }
}

