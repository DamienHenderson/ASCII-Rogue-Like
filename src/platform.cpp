#include "platform.h"

#ifdef __linux__
int _kbhit()
{
    static const int STDIN = 0;
    static bool initialized = false;

    if (! initialized)
    {
        // Use termios to turn off line buffering
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);
    return bytesWaiting;
}


    // Keycodes for Arrow keys
    /*
    up - "\033[A"
    down - "\033[B"
    left - "\033[D"
    right - "\033[C"
    */

#endif
