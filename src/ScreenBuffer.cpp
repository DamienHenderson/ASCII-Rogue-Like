#include "ScreenBuffer.h"

ScreenBuffer::ScreenBuffer(std::vector<std::string> initialbuffer)
{
    buffer = initialbuffer;
    //ctor
}

ScreenBuffer::~ScreenBuffer()
{
    //dtor
}
void ScreenBuffer::EditChar(int2 index,char newchar)
{
    buffer.at(index.y).at(index.x) = newchar;
}
char ScreenBuffer::GetChar(int2 index)
{
    return buffer.at(index.y).at(index.x);
}
std::vector<std::string> ScreenBuffer::GetBuffer()
{
    return buffer;
}
