#include "Actor.h"

Actor::Actor(int2 initialposition, int startinghealth, char character)
{
    //ctor
    this->position = initialposition;
    this->health = startinghealth;
    this->Actor_char = character;

}

Actor::~Actor()
{
    //dtor
}
int2 Actor::GetPosition()
{
    return this->position;
}
void Actor::SetPostion(int2 newpos)
{
    this->position = newpos;
}
void Actor::SetHealth(int newhealth)
{
    this->health = newhealth;
}
int  Actor::GetHealth()
{
    return this->health;
}
char Actor::GetActorChar()
{
    return this->Actor_char;
}
void Actor::SetActorChar(char newchar)
{
    this->Actor_char = newchar;
}
