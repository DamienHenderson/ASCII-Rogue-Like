
#include "types.h"

bool operator < (const int2 &l, const int2 &r)
{
return l.x < r.x && l.y < r.y;
}
