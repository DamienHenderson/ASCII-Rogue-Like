#include "Keys.h"

Keys Convert_Char_To_Keys_Enum(char in)
{

    switch (in)
    {
    case 'q':
        return Keys::q;
        break;
    case 'Q':
        return Keys::Q;
        break;
    case 'w':
        return Keys::w;
        break;
    case 'W':
        return Keys::W;
        break;
    case 'e':
        return Keys::e;
        break;
    case 'E':
        return Keys::E;
        break;
    case 'r':
        return Keys::r;
        break;
    case 'R':
        return Keys::R;
        break;
    case 't':
        return Keys::t;
        break;
    case 'T':
        return Keys::T;
        break;
    case 'y':
        return Keys::y;
        break;
    case 'Y':
        return Keys::Y;
        break;
    case 'u':
        return Keys::u;
        break;
    case 'U':
        return Keys::U;
        break;
    case 'i':
        return Keys::i;
        break;
    case 'I':
        return Keys::I;
        break;
    case 'o':
        return Keys::o;
        break;
    case 'O':
        return Keys::O;
        break;
    case 'p':
        return Keys::p;
        break;
    case 'P':
        return Keys::P;
        break;
    case 'a':
        return Keys::a;
        break;
    case 'A':
        return Keys::A;
        break;
    case 's':
        return Keys::s;
        break;
    case 'S':
        return Keys::S;
        break;
    case 'd':
        return Keys::d;
        break;
    case 'D':
        return Keys::D;
        break;
    case 'f':
        return Keys::f;
        break;
    case 'F':
        return Keys::F;
        break;
    case 'g':
        return Keys::g;
        break;
    case 'G':
        return Keys::G;
        break;
    case 'h':
        return Keys::h;
        break;
    case 'H':
        return Keys::H;
        break;
    case 'j':
        return Keys::j;
        break;
    case 'J':
        return Keys::J;
        break;
    case 'k':
        return Keys::k;
        break;
    case 'K':
        return Keys::K;
        break;
    case 'l':
        return Keys::l;
        break;
    case 'L':
        return Keys::L;
        break;
    case 'z':
        return Keys::z;
        break;
    case 'Z':
        return Keys::Z;
        break;
    case 'x':
        return Keys::x;
        break;
    case 'X':
        return Keys::X;
        break;
    case 'c':
        return Keys::c;
        break;
    case 'C':
        return Keys::C;
        break;
    case 'v':
        return Keys::v;
        break;
    case 'V':
        return Keys::V;
        break;
    case 'b':
        return Keys::b;
        break;
    case 'B':
        return Keys::B;
        break;
    case 'n':
        return Keys::n;
        break;
    case 'N':
        return Keys::N;
        break;
    case 'm':
        return Keys::m;
        break;
    case 'M':
        return Keys::M;
        break;
    default:
        return Keys::Unknown;
        break;
    }
}
