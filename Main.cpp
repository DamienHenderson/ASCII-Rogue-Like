#include "common.h"
#include "Lua/lua.hpp"
#include "platform.h"
#include "Game.h"
#include "Colours.h"
#include "ScreenBuffer.h"
#include "Tile.h"



/*
    TODO:
        - Add Random Map Generation
        - Add Enemies
        - Use Tile Structure for Map Data
        - Add Lua Scripting support
        -
*/

std::vector<std::string> LoadMapFile(const std::string& filename);
void printchar(char ch);

int main(int argc, char ** argv)
{

    // ToDo: add lua scripting
    lua_State * state = luaL_newstate();
    luaL_openlibs(state);
    std::vector<std::string> data = LoadMapFile("test");
    Game* game = new Game(data);
    //std::cout << "Tile Struct is " << sizeof(Tile) << " Bytes wide.";
    //char tmp;
    //std::cin >> tmp;
    while(game->GetGameIsRunning())
    {
        game->Run();
    }

    /*char maze[10][10]
    {
        { 'W','W','W','W','W','W','W','W','W','W' },//Row 0
        { 'W',' ',' ',' ',' ','W','G',' ',' ','W' },//Row 1
        { 'W',' ','W','W','W','W','W','W',' ','W' },//Row 2
        { 'W',' ','W',' ','W',' ',' ','W',' ','W' },//Row 3
        { 'W',' ','W',' ','W',' ','W','W',' ','W' },//Row 4
        { 'W',' ','W',' ','W',' ','W',' ',' ','W' },//Row 5
        { 'W',' ','W',' ','W',' ','W',' ','W','W' },//Row 6
        { 'W',' ','W',' ','W',' ','W',' ','W','W' },//Row 7
        { 'W','P',' ',' ',' ',' ',' ',' ','W','W' },//Row 8
        { 'W','W','W','W','W','W','W','W','W','W' } //Row 9
    };
    int fog[10][10]
    {
        { 0,0,0,0,0,0,0,0,0,0 },//Row 0
        { 0,0,0,0,0,0,0,0,0,0 },//Row 1
        { 0,0,0,0,0,0,0,0,0,0 },//Row 2
        { 0,0,0,0,0,0,0,0,0,0 },//Row 3
        { 0,0,0,0,0,0,0,0,0,0 },//Row 4
        { 0,0,0,0,0,0,0,0,0,0 },//Row 5
        { 0,0,0,0,0,0,0,0,0,0 },//Row 6
        { 0,0,0,0,0,0,0,0,0,0 },//Row 7
        { 0,0,0,0,0,0,0,0,0,0 },//Row 8
        { 0,0,0,0,0,0,0,0,0,0 } //Row 9
    };

    //printmaze(mazestr);
    int player_row {-1}, player_col{-1};
    // Find the player's start position
    for(int x = 0; x < 10 && player_row == -1; x++)
    {
        //std::cout << "Row: " << x << std::endl;
        for (int y = 0; y < 10; y++ )
        {
            //std::cout << "Column: " << y << std::endl;
            if(maze[x][y] == 'P')
            {
                player_row = x;
                player_col = y;
                break;
            }
        }
    }

    if(player_row == -1)
    {
        std::cout << "Player could not be found" << std::endl;
        return 1;
    }
    std::cout << "Player position X: " << player_row << " Y: " << player_col << std::endl;
    // Delete player from array
    maze[player_row][player_col] = ' ';
    playgame(maze,fog, player_row, player_col);
    */
    lua_close(state);
    return 0;
}
std::vector<std::string> LoadMapFile(const std::string& filename)
{
    // TODO: Make Files UTF-8 and load special characters to allow more than 127 types of tiles
    std::vector<std::string> mapdataout;

    std::fstream file;
    file.open(filename,std::ios::in);


    std::string line;
    while (!file.eof())
    {

        std::getline(file,line);
        line.append(1,'\n');
        mapdataout.push_back(line);



    }
    file.close();
    std::cin >> std::skipws;
    return mapdataout;
}



/*void playgame(char maze[10][10], int fog[10][10], int player_row, int player_col)
{

    int walked_into_walls_count = 0;
    bool goalfound = false;
    while(!goalfound)
    {
        CLEARSCREEN
        updatefogofwar(fog,player_row,player_col);
        printmazefogofwar(maze,fog);
        // game loop

        std::cout << "What do you want to do? " << std::endl;
        char option = ' ';
        bool invalid_option = false;
        do
        {
            std::cout << "[N]orth, [E]ast, [S]outh, [w]est,[L]ook: ";
            option = getchar();
            switch(option)
            {
            case 'n':
            case 'N':
            {
                if(player_row == 0)
                {
                    std::cout << "You are at the edge of the world." << std::endl;
                }
                else if(maze[player_row - 1][player_col] == 'W' && walked_into_walls_count > 5)
                {
                    std::cout << "Stop walking into walls you stupid git." << std::endl;
                    walked_into_walls_count++;
                }
                else if(maze[player_row - 1][player_col] == 'W')
                {
                    std::cout << "There is a wall there" << std::endl;
                    walked_into_walls_count++;
                }
                else
                {
                    maze[player_row][player_col] = ' ';
                    player_row--;

                }
                break;
            }
            case 'e':
            case 'E':
            {
                if(player_col == 9)
                {
                    std::cout << "You are at the edge of the world." << std::endl;
                }
                else if(maze[player_row][player_col + 1] == 'W' && walked_into_walls_count > 5)
                {
                    std::cout << "Stop walking into walls you stupid git." << std::endl;
                    walked_into_walls_count++;
                }
                else if(maze[player_row][player_col + 1] == 'W')
                {
                    std::cout << "There is a wall there" << std::endl;
                    walked_into_walls_count++;
                }
                else
                {
                    maze[player_row][player_col] = ' ';
                    player_col++;

                }
                break;

            }
            case 's':
            case 'S':
            {
                if(player_row == 9)
                {
                    std::cout << "You are at the edge of the world." << std::endl;
                }
                else if(maze[player_row + 1][player_col] == 'W' && walked_into_walls_count > 5)
                {
                    std::cout << "Stop walking into walls you stupid git." << std::endl;
                    walked_into_walls_count++;
                }
                else if(maze[player_row + 1][player_col] == 'W')
                {
                    std::cout << "There is a wall there" << std::endl;
                    walked_into_walls_count++;
                }
                else
                {
                    maze[player_row][player_col] = ' ';
                    player_row++;

                }
                break;

            }
            case 'w':
            case 'W':
            {
                if(player_col == 0)
                {
                    std::cout << "You are at the edge of the world." << std::endl;
                }
                else if(maze[player_row][player_col - 1] == 'W' && walked_into_walls_count > 5)
                {
                    std::cout << "Stop walking into walls you stupid git." << std::endl;
                    walked_into_walls_count++;
                }
                else if(maze[player_row][player_col - 1] == 'W')
                {
                    std::cout << "There is a wall there" << std::endl;
                    walked_into_walls_count++;
                }
                else
                {
                    maze[player_row][player_col] = ' ';
                    player_col--;


                }
                break;
            }
            case 'l':
            case 'L':
            {
                std::cout << "North: ";
                printmazecell(maze,player_row -1, player_col);
                std::cout << "South: ";
                printmazecell(maze,player_row +1, player_col);
                std::cout << "East: ";
                printmazecell(maze,player_row, player_col + 1);
                std::cout << "West: ";
                printmazecell(maze,player_row, player_col - 1);

                break;
            }
            default:
            {
                invalid_option = true;
            }

            }
        }
        while(invalid_option);
        if (maze[player_row][player_col] == 'G')
        {
            std::cout << "You have reached the goal" << std::endl;

        }
        else
        {
            maze[player_row][player_col] = 'P';
        }

    }

}
int convertxytostringposition(int x, int y, int linewidth)
{
    return x + (y * linewidth);
}
void writemap(const std::string & filename, const std::string & mapdata)
{
    std::ofstream fileout;
    fileout.open(filename, std::ofstream::out);
    fileout.write(mapdata.c_str(),sizeof(std::string));
    fileout.close();
}
std::string loadmapfile(const std::string& filename)
{
    std::ifstream file(filename, std::ifstream::in);
    std::ostringstream filestream;

    // preserve newlines
    filestream << file.rdbuf();
    std::string filedata = filestream.str();
    file.close();
    return filedata;
}
void resetfog(int fog[10][10])
{
    for(int row = 0; row < 10; row++)
    {
        for(int column = 0; column < 10; column++)
        {
            fog[row][column] = 0;
        }
    }
}
void printmazefogofwar(char maze[10][10], int fogbitmask[10][10] )
{
    for(int row = 0; row < 10; row++)
    {
        std::cout << std::endl;
        for(int column = 0; column < 10; column++)
        {
            if (maze[row][column] == 'P' && fogbitmask[row][column] == 1)
            {
                std::cout << "@";
            }

            else if (maze[row][column] == 'W' && fogbitmask[row][column] == 1)
            {
                std::cout << "#";
            }
            else if (maze[row][column] == ' ' && fogbitmask[row][column] == 1)
            {
                std::cout << "-";
            }
            else if (maze[row][column] == 'G' && fogbitmask[row][column] == 1)
            {
                std::cout << "*";
            }
            else if ( fogbitmask[row][column] == 0)
            {
                std::cout << "+";
            }
        }
    }
    std::cout << std::endl;
}
void updatefogofwar(int fog[10][10], int player_x,int  player_y)
{
    //Player position
    fog[player_x][player_y] = 1;

    // North and South
    fog[player_x][player_y + 1] = 1;
    fog[player_x][player_y - 1] = 1;

    // East and West
    fog[player_x + 1][player_y] = 1;
    fog[player_x - 1][player_y] = 1;

    // North East and North West
    fog[player_x + 1][player_y - 1] = 1;
    fog[player_x - 1][player_y - 1] = 1;

    // South East and South West
    fog[player_x - 1][player_y + 1] = 1;
    fog[player_x + 1][player_y + 1] = 1;
}
void printmazecell(char maze[10][10],int row, int column)
{
    if (maze[row][column] == 'P')
    {
        std::cout << "@";
    }

    else if (maze[row][column] == 'W')
    {
        std::cout << "#";
    }
    else if (maze[row][column] == ' ')
    {
        std::cout << "-";
    }
    else if (maze[row][column] == 'G')
    {
        std::cout << "*";
    }
    std::cout << std::endl;
}
void printmaze(char maze[10][10])
{
    for (int row = 0; row < 10; row++)
    {
        std::cout << std::endl;
        for (int column = 0; column < 10; column++)
        {
            if (maze[row][column] == 'P')
            {
                std::cout << "@";
            }

            else if (maze[row][column] == 'W')
            {
                std::cout << "#";
            }
            else if (maze[row][column] == ' ' || maze[row][column] == 'G')
            {
                std::cout << "-";
            }

        }
    }
    std::cout << std::endl;
}
void printmaze(const std::string & str)
{
    int row = 0;
    int column = 0;
    while (row < 10)
    {


        for(char c : str)
        {
            if(row >= 10)
            {
                break;
            }
            if (c == '\0')
            {
                return;
            }
            if (column >= 10)
            {
                column = 0;
                std::cout << std::endl;
                row++;
            }
            if (c == 'P')
            {
                std::cout << "@";
            }

            else if (c == 'W')
            {
                std::cout << "#";
            }
            else if (c == ' ' || c == ' ')
            {
                std::cout << "-";
            }
            else if (c == '\n')
            {
                if (column < 9)
                {
                    //for (int i = column; i < 10; i++)
                    //{
                    // fill rest of line with dashes
                    //   std::cout << "-";
                    // }
                    row++;
                }
                std::cout << '\n';

            }
            //std::cout << column << std::endl;
        }

    }

}
void printmazefogofwar(const std::string& mapstring, int row, int column)
{

}
*/

