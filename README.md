# ASCII-Rogue-Like
An ASCII Roguelike written in C++ using C++11 features. Primarily developed for Linux however Windows versions will become available eventually.

Running:
	
	Linux:	
		To Run the software either open the included codeblocks project file and
		build and run the project or run the included executable file. 

	Windows:
		Windows Support is currently unavailable however Windows support is
		intended for a future release.

	All Platforms:
		This Game currently has no dependencies other than the included Lua C
		API source files which were not developed by me.

		More information about Lua can be found at www.lua.org
Lua Modding:

	Currently it is planned to support full Lua modding of this game however Lua support is not currently implemented and will probably come after Windows support.

About the Game:

	The Game is currently unnamed however it is intended to be an Angband-like Sci-Fi Rogue-like with combat and an in game economy.

